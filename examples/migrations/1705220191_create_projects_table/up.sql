/*
 * mup
 * Copyright (C) 2024-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

CREATE TABLE projects
(
    id       UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    name     VARCHAR(255) NOT NULL,
    owner_id UUID         NOT NULL,
    CONSTRAINT projects_owner_user_fk
        FOREIGN KEY (owner_id)
            REFERENCES users (id) ON DELETE SET NULL
);

CREATE UNIQUE INDEX projects_owner_name_unique_index ON projects (name, owner_id);
