FROM registry.gitlab.com/rahome/docker/debian:bookworm-slim
LABEL maintainer="Tobias Raatiniemi <raatiniemi@gmail.com>"

ENV RUST_LOG="info"

RUN set -x \
  && groupadd mup \
  && useradd -g mup -m -d /etc/mup mup

COPY target/x86_64-unknown-linux-gnu/release/mup /usr/local/bin

USER mup:mup
VOLUME /etc/mup

HEALTHCHECK CMD true

WORKDIR /etc/mup

ENTRYPOINT ["mup"]
CMD ["--help"]
