/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::config::Config;
use crate::migration::harness::MigrationHarness;
use crate::migration::name::MigrationName;
use log::debug;
use std::fmt::{Display, Formatter};

mod driver;
pub(crate) mod harness;
pub(super) mod migrator;
pub(crate) mod name;
pub(crate) mod source;

/// Define the scheme used with PostgreSQL configuration.
const SCHEME_POSTGRES: &str = "postgres://";

#[derive(Debug, PartialEq)]
enum Error {
    UnsupportedDatabaseConfiguration,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnsupportedDatabaseConfiguration => {
                write!(
                    f,
                    "The database defined by the configuration is not supported, \
                     only PostgreSQL is supported using the {} scheme",
                    SCHEME_POSTGRES
                )
            }
        }
    }
}

impl std::error::Error for Error {}

/// Define the shared properties used by all types of migrations.
trait Migration {
    fn name(&self) -> &MigrationName;

    fn checksum(&self) -> String;
}

pub(crate) fn migration_harness(
    config: &Config,
) -> Result<MigrationHarness, Box<dyn std::error::Error>> {
    let database_url = config.database_url()?;
    if database_url.starts_with(SCHEME_POSTGRES) {
        debug!("Detect PostgreSQL usage based on database url configuration");
        Ok(MigrationHarness::new(&database_url))
    } else {
        Err(Box::new(Error::UnsupportedDatabaseConfiguration))
    }
}
