/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt::{Display, Formatter};
use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};

use crate::cli::command::Command;
use crate::migration::name::MigrationName;
use async_trait::async_trait;
use log::{debug, info};

/// Define the name of the command, used for logging and reporting.
const COMMAND_NAME: &str = "create";

#[derive(Debug, PartialEq)]
enum Error {
    InvalidMigrationsPath(PathBuf),
    MigrationAlreadyExists(String),
    UnableToCreateMigrationDirectory(PathBuf, std::io::ErrorKind),
    UnableToCreateMigrationFile(PathBuf, std::io::ErrorKind),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InvalidMigrationsPath(base_path) => {
                write!(
                    f,
                    "The migrations path {:?} is invalid, path to directory is expected",
                    base_path
                )
            }
            Error::MigrationAlreadyExists(migration_name) => {
                write!(
                    f,
                    "A migration with name \"{}\" already exists, unable to override",
                    migration_name
                )
            }
            Error::UnableToCreateMigrationDirectory(path, e) => {
                write!(f, "Unable to create migration path {:?} ({})", path, e)
            }
            Error::UnableToCreateMigrationFile(path, e) => {
                write!(f, "Unable to create migration script {:?} ({})", path, e)
            }
        }
    }
}

impl std::error::Error for Error {}

/// Represent the command for creating a new migration.
pub(crate) struct CreateCommand {
    base_path: PathBuf,
    migration_name: MigrationName,
}

impl CreateCommand {
    pub(crate) fn new(
        base_path: &Path,
        description: &str,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let version = SystemTime::now().duration_since(UNIX_EPOCH)?.as_secs() as i64;

        debug!("Creating {}-command", COMMAND_NAME);
        let command = CreateCommand {
            base_path: base_path.to_path_buf(),
            migration_name: MigrationName::new(version, description),
        };
        Ok(command)
    }
}

#[async_trait]
impl Command for CreateCommand {
    async fn run(&self) -> Result<(), Box<dyn std::error::Error>> {
        ensure_migrations_path_is_valid(self.base_path.as_path())
            .and_then(|base_path| create_migration_path(base_path, &self.migration_name))
            .map_err(|e| e.into())
    }
}

fn ensure_migrations_path_is_valid(base_path: &Path) -> Result<&Path, Error> {
    debug!("Check whether migrations path exists");
    if base_path.exists() {
        if base_path.is_dir() {
            debug!("Migrations path is a valid directory");
            return Ok(base_path);
        }
        let path = base_path.to_path_buf();
        return Err(Error::InvalidMigrationsPath(path));
    }

    debug!("Migrations path do not exists, will be created...");
    Ok(base_path)
}

fn create_migration_path(base_path: &Path, migration_name: &MigrationName) -> Result<(), Error> {
    let name = sanitize_name(&format!("{}", migration_name));
    let migration_path = base_path.join(&name);

    if migration_path.exists() {
        return Err(Error::MigrationAlreadyExists(name));
    }

    create_migration_directory(&migration_path).and_then(|_| {
        create_migration_script(migration_path).map(|_| {
            info!("Migration \"{}\" was created", name);
        })
    })
}

fn sanitize_name(name: &str) -> String {
    name.replace(" ", "_").to_lowercase()
}

fn create_migration_directory(migration_path: &PathBuf) -> Result<(), Error> {
    fs::create_dir_all(migration_path).map_err(|e| {
        Error::UnableToCreateMigrationDirectory(migration_path.to_path_buf(), e.kind())
    })?;

    debug!("Creating migration directories");
    Ok(())
}

fn create_migration_script(migration_path: PathBuf) -> Result<(), Error> {
    let script_path = migration_path.join("up.sql");
    File::create(&script_path)
        .map_err(|e| Error::UnableToCreateMigrationFile(script_path, e.kind()))?;

    Ok(())
}

#[cfg(test)]
mod tests {
    extern crate tempfile;

    use std::fs;
    use std::fs::File;
    use std::path::{Path, PathBuf};

    use tempfile::TempDir;

    use super::*;

    const MIGRATIONS: &str = "migrations";

    fn create_tempdir(prefix: &str) -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = TempDir::with_prefix(prefix).map(|x| x.into_path())?;
        Ok(path)
    }

    fn create_dir(path: &Path) -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = fs::create_dir(path).map(|_| path.to_path_buf())?;
        Ok(path)
    }

    fn create_file(path: &Path) -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = File::create(path).map(|_| path.to_path_buf())?;
        Ok(path)
    }

    #[tokio::test]
    async fn run_without_permission_to_directory() {
        let tempdir = PathBuf::from("/");
        let migrations_path = tempdir.join(MIGRATIONS);
        let command = CreateCommand {
            base_path: migrations_path,
            migration_name: MigrationName::new(1, "a"),
        };

        let actual = command.run().await;

        assert!(actual.is_err());
    }

    #[tokio::test]
    async fn run_with_file_as_base_path() -> Result<(), Box<dyn std::error::Error>> {
        let tempdir = create_tempdir(MIGRATIONS)?;
        let file = &tempdir.join("file");
        let base_path = create_file(file)?;
        let command = CreateCommand {
            base_path,
            migration_name: MigrationName::new(1, "a"),
        };

        let actual = command.run().await;

        assert!(actual.is_err());
        Ok(())
    }

    #[tokio::test]
    async fn run_when_migration_exists() -> Result<(), Box<dyn std::error::Error>> {
        let tempdir = create_tempdir(MIGRATIONS)?;
        let _ = create_dir(&tempdir.join("1_a"))?;
        let command = CreateCommand {
            base_path: tempdir,
            migration_name: MigrationName::new(1, "a"),
        };

        let actual = command.run().await;

        assert!(actual.is_err());
        Ok(())
    }

    #[tokio::test]
    async fn run_with_sanitize_migration_name() -> Result<(), Box<dyn std::error::Error>> {
        let tempdir = create_tempdir(MIGRATIONS)?;
        let migration_path = tempdir.join("1_a_b_c_d");
        let up_path = migration_path.join("up.sql");
        let command = CreateCommand {
            base_path: tempdir,
            migration_name: MigrationName::new(1, "a B c D"),
        };

        let actual = command.run().await;

        assert!(actual.is_ok());
        assert!(migration_path.is_dir());
        assert!(up_path.is_file());
        Ok(())
    }

    #[tokio::test]
    async fn run_without_migrations_path() -> Result<(), Box<dyn std::error::Error>> {
        let tempdir = create_tempdir("without")?;
        let migrations_path = tempdir.join(MIGRATIONS);
        let migration_path = migrations_path.join("1_a");
        let up_path = migration_path.join("up.sql");
        let command = CreateCommand {
            base_path: migrations_path,
            migration_name: MigrationName::new(1, "a"),
        };

        let actual = command.run().await;

        assert!(actual.is_ok());
        assert!(migration_path.is_dir());
        assert!(up_path.is_file());
        Ok(())
    }

    #[tokio::test]
    async fn run_without_migration() -> Result<(), Box<dyn std::error::Error>> {
        let tempdir = create_tempdir(MIGRATIONS)?;
        let migration_path = tempdir.join("1_a");
        let up_path = migration_path.join("up.sql");
        let command = CreateCommand {
            base_path: tempdir,
            migration_name: MigrationName::new(1, "a"),
        };

        let actual = command.run().await;

        assert!(actual.is_ok());
        assert!(migration_path.is_dir());
        assert!(up_path.is_file());
        Ok(())
    }
}
