/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::cli::command::Command;
use crate::config::Config;
use crate::migration::migration_harness;
use crate::migration::source::migration_source;
use async_trait::async_trait;
use log::debug;

/// Define the name of the command, used for logging and reporting.
const COMMAND_NAME: &str = "up";

/// Represent the command for running the migrations.
pub(crate) struct UpCommand {
    config: Config,
}

impl UpCommand {
    pub(crate) fn new(config: Config) -> Self {
        debug!("Creating {}-command", COMMAND_NAME);
        UpCommand { config }
    }
}

#[async_trait]
impl Command for UpCommand {
    async fn run(&self) -> Result<(), Box<dyn std::error::Error>> {
        let migration_source = migration_source(&self.config);
        let available_migrations = migration_source.migrations()?;
        if available_migrations.is_empty() {
            debug!("No migrations are available");
            return Ok(());
        }

        let harness = migration_harness(&self.config)?;
        let mut migrator = harness.setup().await?;
        migrator.apply(available_migrations).await
    }
}
