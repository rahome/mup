/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use async_trait::async_trait;

use crate::cli;
use crate::cli::create_command::CreateCommand;
use crate::cli::up_command::UpCommand;
use crate::cli::CommandLine;
use crate::config::Config;

/// Represents an available command that the user can run.
#[async_trait]
pub(super) trait Command {
    /// Run the actual command.
    async fn run(&self) -> Result<(), Box<dyn std::error::Error>>;
}

/// Attempt to create the command specified by the user.
pub(super) fn create_command(
    cli: CommandLine,
    config: Config,
) -> Result<Box<dyn Command>, Box<dyn std::error::Error>> {
    let command: Box<dyn Command> = match cli.command {
        cli::Commands::Create(args) => {
            let create_command = CreateCommand::new(config.path(), &args.description)?;
            Box::new(create_command)
        }
        cli::Commands::Up(_) => {
            let up_command = UpCommand::new(config);
            Box::new(up_command)
        }
    };
    Ok(command)
}
