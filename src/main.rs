/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::process::exit;

use log::{debug, error};

mod cli;
mod config;
mod migration;

/// Define the name of the application.
const NAME: &str = "mup";

#[tokio::main]
async fn main() {
    env_logger::init();

    match cli::run().await {
        Ok(_) => {
            debug!("Command was successful");
            exit(0)
        }
        Err(e) => {
            error!("{}", e);
            exit(1)
        }
    }
}
