/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt::{Display, Formatter};
use std::str::FromStr;
use std::time::{Duration, SystemTime};

use crate::migration::driver::migration::AppliedMigration;
use crate::migration::source::migration::AvailableMigration;
use crate::migration::{Migration, MigrationName};
use log::debug;
use sqlx::postgres::{PgConnectOptions, PgRow};
use sqlx::{query, ConnectOptions, Connection, Executor, PgConnection, Postgres, Row, Transaction};

const CHECK_IF_MIGRATIONS_TABLE_EXISTS: &str = r#"
SELECT tablename
FROM pg_catalog.pg_tables
WHERE schemaname = "current_schema"() AND lower(tablename) = lower('__migrations')
LIMIT 1;
"#;

const CREATE_TABLE_MIGRATIONS: &str = r#"
CREATE TABLE __migrations (
    version                 BIGINT      PRIMARY KEY,
    description             TEXT        NOT NULL,
    checksum                VARCHAR(64) NOT NULL,
    duration_in_nanoseconds INT         NOT NULL
);
"#;

const FIND_MIGRATIONS: &str = r#"
SELECT version, description, checksum, duration_in_nanoseconds FROM __migrations ORDER BY version
"#;

const ADD_APPLIED_MIGRATION: &str = r#"
INSERT INTO __migrations(version, description, checksum, duration_in_nanoseconds) VALUES ($1, $2, $3, $4);
"#;

#[derive(Debug, PartialEq)]
enum Error {
    DatabaseNameIsMissingFromConfiguration,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::DatabaseNameIsMissingFromConfiguration => {
                write!(
                    f,
                    "Database name is missing from the database configuration. \
                     The database name should be included in the \"database_url\" \
                     key from the configuration file or in the value for the \
                     \"MUP_DATABASE_URL\" environment variable"
                )
            }
        }
    }
}

impl std::error::Error for Error {}

pub(crate) struct PostgresDriver {
    connection: PgConnection,
}

impl PostgresDriver {
    pub(crate) async fn from(
        database_url: &str,
    ) -> Result<PostgresDriver, Box<dyn std::error::Error>> {
        let connection = create_connection(database_url).await?;
        let driver = PostgresDriver { connection };
        Ok(driver)
    }
}

async fn create_connection(database_url: &str) -> Result<PgConnection, Box<dyn std::error::Error>> {
    debug!("Parsing connection options");
    let options = PgConnectOptions::from_str(database_url)?;

    debug!("Checking whether connect options contain database name");
    match options.get_database() {
        Some(_) => {
            debug!("Connect options are valid, connecting...");
            connect(options).await
        }
        None => Err(Box::new(Error::DatabaseNameIsMissingFromConfiguration)),
    }
}

async fn connect(options: PgConnectOptions) -> Result<PgConnection, Box<dyn std::error::Error>> {
    let connection = options.connect().await?;
    Ok(connection)
}

impl PostgresDriver {
    pub(crate) async fn check_if_migrations_table_exists(
        &mut self,
    ) -> Result<bool, Box<dyn std::error::Error>> {
        let v = sqlx::query(CHECK_IF_MIGRATIONS_TABLE_EXISTS)
            .fetch_optional(&mut self.connection)
            .await?;

        debug!("Query for migrations table was successful");
        Ok(v.is_some())
    }

    pub(crate) async fn create_migrations_table(
        &mut self,
    ) -> Result<(), Box<dyn std::error::Error>> {
        debug!("Attempt to create migrations table");
        sqlx::query(CREATE_TABLE_MIGRATIONS)
            .execute(&mut self.connection)
            .await?;

        debug!("Create table for migrations was successful");
        Ok(())
    }

    pub(crate) async fn find_migrations(
        &mut self,
    ) -> Result<Vec<AppliedMigration>, Box<dyn std::error::Error>> {
        let migration_names = query(FIND_MIGRATIONS)
            .fetch_all(&mut self.connection)
            .await?
            .iter()
            .map(applied_migration)
            .collect();

        Ok(migration_names)
    }

    pub(crate) async fn run_migration(
        &mut self,
        pending_migration: &AvailableMigration,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut tx = Connection::begin(&mut self.connection).await?;
        let duration = run_migration(&mut tx, pending_migration).await?;

        let applied_migration = AppliedMigration::from(pending_migration, duration);
        add_applied_migration(&mut tx, &applied_migration).await?;
        tx.commit().await?;
        Ok(())
    }
}

fn applied_migration(row: &PgRow) -> AppliedMigration {
    let version: i64 = row.get("version");
    let description: &str = row.get("description");
    let duration_in_nanoseconds: i32 = row.get("duration_in_nanoseconds");
    AppliedMigration::new(
        &MigrationName::new(version, description),
        row.get("checksum"),
        Duration::from_nanos(duration_in_nanoseconds as u64),
    )
}

async fn run_migration(
    tx: &mut Transaction<'_, Postgres>,
    pending_migration: &AvailableMigration,
) -> Result<Duration, Box<dyn std::error::Error>> {
    let start = SystemTime::now();
    tx.execute(pending_migration.content()).await?;
    let end = SystemTime::now();

    let duration = end.duration_since(start)?;
    Ok(duration)
}

async fn add_applied_migration(
    tx: &mut Transaction<'_, Postgres>,
    applied_migration: &AppliedMigration,
) -> Result<(), Box<dyn std::error::Error>> {
    let migration_name = applied_migration.name();
    query(ADD_APPLIED_MIGRATION)
        .bind(migration_name.version())
        .bind(migration_name.description())
        .bind(applied_migration.checksum())
        .bind(applied_migration.duration())
        .execute(&mut **tx)
        .await?;

    Ok(())
}
