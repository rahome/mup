/*
 * mup
 * Copyright (C) 2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::migration::name::MigrationName;
use crate::migration::Migration;
use std::cmp::Ordering;
use std::time::Duration;

/// Represents a migration that have been successfully applied.
///
/// The migration will contain the name and checksum for the migration to ensure the integrity of
/// the migrations, along with the duration for how long the migration took to apply.
#[derive(Debug, Eq, PartialEq, Clone)]
pub(crate) struct AppliedMigration {
    name: MigrationName,
    checksum: String,
    duration: Duration,
}

impl AppliedMigration {
    pub(crate) fn duration(&self) -> i32 {
        self.duration.as_nanos() as i32
    }
}

impl AppliedMigration {
    pub(crate) fn new(name: &MigrationName, checksum: &str, duration: Duration) -> Self {
        AppliedMigration {
            name: name.clone(),
            checksum: checksum.to_string(),
            duration,
        }
    }

    pub(crate) fn from(migration: &impl Migration, duration: Duration) -> Self {
        AppliedMigration::new(migration.name(), &migration.checksum(), duration)
    }
}

impl Migration for AppliedMigration {
    fn name(&self) -> &MigrationName {
        &self.name
    }

    fn checksum(&self) -> String {
        self.checksum.clone()
    }
}

impl Ord for AppliedMigration {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for AppliedMigration {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
#[macro_use]
mod tests {
    use crate::migration::driver::migration::AppliedMigration;
    use crate::migration::name::MigrationName;
    use std::time::Duration;

    extern crate quickcheck;

    #[test]
    fn sort_by_version() {
        let mut actual = vec![
            AppliedMigration::new(&MigrationName::new(8, "h"), "h", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(4, "d"), "d", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(10, "j"), "j", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(5, "e"), "e", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(6, "f"), "f", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(7, "g"), "g", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(2, "b"), "b", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(9, "i"), "i", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(3, "c"), "c", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(1, "a"), "a", Duration::from_millis(1)),
        ];
        let expected = vec![
            AppliedMigration::new(&MigrationName::new(1, "a"), "a", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(2, "b"), "b", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(3, "c"), "c", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(4, "d"), "d", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(5, "e"), "e", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(6, "f"), "f", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(7, "g"), "g", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(8, "h"), "h", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(9, "i"), "i", Duration::from_millis(1)),
            AppliedMigration::new(&MigrationName::new(10, "j"), "j", Duration::from_millis(1)),
        ];

        actual.sort();

        assert_eq!(expected, actual);
    }
}
