/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use log::debug;

use crate::migration::driver::postgres::PostgresDriver;
use crate::migration::migrator::Migrator;

pub(crate) struct MigrationHarness {
    database_url: String,
}

impl MigrationHarness {
    pub(super) fn new(database_url: &str) -> Self {
        MigrationHarness {
            database_url: database_url.to_string(),
        }
    }
}

impl MigrationHarness {
    pub(crate) async fn setup(&self) -> Result<Migrator, Box<dyn std::error::Error>> {
        let mut driver = PostgresDriver::from(&self.database_url).await?;
        create_migrations_table_if_missing(&mut driver).await?;
        let migrations = driver.find_migrations().await?;

        Ok(Migrator::new(driver, migrations))
    }
}

async fn create_migrations_table_if_missing(
    driver: &mut PostgresDriver,
) -> Result<(), Box<dyn std::error::Error>> {
    let table_exists = driver.check_if_migrations_table_exists().await?;
    if table_exists {
        debug!("Table for migrations was found, no need to create it");
        Ok(())
    } else {
        debug!("Table for migrations was not found, creating...");
        driver.create_migrations_table().await
    }
}

#[cfg(test)]
mod tests {
    use std::future::Future;
    use std::str::FromStr;

    use crate::config::Config;
    use crate::migration::driver::migration::AppliedMigration;
    use crate::migration::driver::postgres::PostgresDriver;
    use crate::migration::harness::MigrationHarness;
    use crate::migration::source::migration::AvailableMigration;
    use crate::migration::source::script::Script;
    use crate::migration::MigrationName;
    use sqlx::postgres::PgConnectOptions;
    use sqlx::{ConnectOptions, Connection, PgConnection};
    use uuid::Uuid;

    fn replace_database_name(
        config: &Config,
        database_name: &str,
    ) -> Result<String, Box<dyn std::error::Error>> {
        config
            .database_url()
            .map(|x| x.replace("/mup", &format!("/{}", database_name)))
    }

    fn generate_unique_database_name() -> String {
        format!("mup_{}", Uuid::new_v4()).replace("-", "_")
    }

    async fn setup_and_tear_down_database(
        config: &Config,
        database_name: &str,
        producer: impl Future<Output = Result<(), Box<dyn std::error::Error>>>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let database_url = config.database_url()?;
        let options = PgConnectOptions::from_str(&database_url)?;
        let mut c = options.connect().await?;
        let user = "postgres";
        create_database(&mut c, database_name, user).await?;
        grant_privileges(&mut c, database_name, user).await?;

        let result = producer.await;

        drop_database(&mut c, database_name).await?;
        c.close().await?;
        result
    }

    async fn create_database(
        c: &mut PgConnection,
        database_name: &str,
        user: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        sqlx::query(&format!(
            r#"CREATE DATABASE "{database_name}" WITH OWNER = "{user}""#
        ))
        .execute(c)
        .await?;
        Ok(())
    }

    async fn grant_privileges(
        c: &mut PgConnection,
        database_name: &str,
        user: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        sqlx::query(&format!(
            r#"GRANT ALL PRIVILEGES ON DATABASE "{database_name}" TO "{user}""#
        ))
        .execute(c)
        .await?;
        Ok(())
    }

    async fn drop_database(
        c: &mut PgConnection,
        database_name: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        sqlx::query(&format!(r#"DROP DATABASE IF EXISTS "{database_name}""#))
            .execute(c)
            .await?;
        Ok(())
    }

    async fn add_migrations(
        database_url: &str,
        available_migrations: &Vec<AvailableMigration>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut driver = PostgresDriver::from(database_url).await?;
        for available_migration in available_migrations {
            driver.run_migration(available_migration).await?;
        }
        Ok(())
    }

    async fn migrations_table_exists(
        database_url: &str,
    ) -> Result<bool, Box<dyn std::error::Error>> {
        let mut driver = PostgresDriver::from(database_url).await?;
        let exists = driver.check_if_migrations_table_exists().await?;
        Ok(exists)
    }

    #[tokio::test]
    async fn setup_without_empty_url() {
        let migration_harness = MigrationHarness::new("");

        let actual = migration_harness.setup().await;

        assert!(actual.is_err())
    }

    #[tokio::test]
    async fn setup_with_missing_database_name() -> Result<(), Box<dyn std::error::Error>> {
        let config = Config::parse(".mup.toml")?;
        let database_url = replace_database_name(&config, "")?;
        let migration_harness = MigrationHarness::new(&database_url);

        let actual = migration_harness.setup().await;

        assert!(actual.is_err());
        Ok(())
    }

    #[tokio::test]
    async fn setup_without_table() -> Result<(), Box<dyn std::error::Error>> {
        let config = Config::parse(".mup.toml")?;
        let database_name = generate_unique_database_name();

        let test = setup_and_tear_down_database(&config, &database_name, async {
            let database_url = replace_database_name(&config, &database_name)?;
            let migration_harness = MigrationHarness::new(&database_url);

            migration_harness.setup().await?;

            assert!(migrations_table_exists(&database_url).await?);
            Ok(())
        });

        test.await
    }

    #[tokio::test]
    async fn setup_without_migrations() -> Result<(), Box<dyn std::error::Error>> {
        let config = Config::parse(".mup.toml")?;
        let database_name = generate_unique_database_name();

        let test = setup_and_tear_down_database(&config, &database_name, async {
            let database_url = replace_database_name(&config, &database_name)?;
            let migration_harness = MigrationHarness::new(&database_url);
            migration_harness.setup().await?;
            let applied_migrations: Vec<AppliedMigration> = vec![];

            let actual = migration_harness.setup().await?;

            assert!(migrations_table_exists(&database_url).await?);
            assert_eq!(applied_migrations, actual.applied_migrations);
            Ok(())
        });

        test.await
    }

    #[tokio::test]
    async fn setup_with_migration() -> Result<(), Box<dyn std::error::Error>> {
        let config = Config::parse(".mup.toml")?;
        let database_name = generate_unique_database_name();

        let test = setup_and_tear_down_database(&config, &database_name, async {
            let database_url = replace_database_name(&config, &database_name)?;
            let migration_harness = MigrationHarness::new(&database_url);
            migration_harness.setup().await?;
            let available_migrations = vec![AvailableMigration::new(
                MigrationName::new(1, "a"),
                Script::from("-- a"),
            )];
            add_migrations(&database_url, &available_migrations).await?;

            let actual = migration_harness.setup().await?;

            assert!(migrations_table_exists(&database_url).await?);
            assert_eq!(1, actual.applied_migrations.len());
            Ok(())
        });

        test.await
    }

    #[tokio::test]
    async fn setup_with_migrations() -> Result<(), Box<dyn std::error::Error>> {
        let config = Config::parse(".mup.toml")?;
        let database_name = generate_unique_database_name();

        let test = setup_and_tear_down_database(&config, &database_name, async {
            let database_url = replace_database_name(&config, &database_name)?;
            let migration_harness = MigrationHarness::new(&database_url);
            migration_harness.setup().await?;
            let available_migrations = vec![
                AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
                AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
                AvailableMigration::new(MigrationName::new(3, "c"), Script::from("-- c")),
                AvailableMigration::new(MigrationName::new(4, "d"), Script::from("-- d")),
            ];
            add_migrations(&database_url, &available_migrations).await?;

            let actual = migration_harness.setup().await?;

            assert!(migrations_table_exists(&database_url).await?);
            assert_eq!(4, actual.applied_migrations.len());
            Ok(())
        });

        test.await
    }
}
