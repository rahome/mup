/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::config::Config;
use crate::migration::source::file::file_migration_source::FileMigrationSource;
use crate::migration::source::migration_source::MigrationSource;

mod file;
pub(crate) mod migration;
pub(crate) mod migration_source;
pub(crate) mod script;

pub(crate) fn migration_source(config: &Config) -> Box<dyn MigrationSource + Send> {
    Box::new(FileMigrationSource::new(config.path()))
}
