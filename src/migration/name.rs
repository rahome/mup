/*
 * mup
 * Copyright (C) 2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt::{Display, Formatter};
use std::num::ParseIntError;

/// Separator between version and description in the migration name.
const MIGRATION_NAME_SEPARATOR: &str = "_";

/// Errors associated with parsing of the migration name.
#[derive(Debug, PartialEq, Eq)]
enum Error {
    /// Indicate that the format for the migration name is invalid.
    UnableToParseWithInvalidFormat(String),
    /// Indicate that we were unable to parse the version part from the name.
    UnableToParseVersion(String, ParseIntError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnableToParseWithInvalidFormat(name) => {
                write!(f, "Format for migration name \"{}\" is invalid, format \"version_description\" is expected", name)
            }
            Error::UnableToParseVersion(v, e) => {
                write!(f, "Parse version from \"{}\" cause: {}", v, e)
            }
        }
    }
}

impl std::error::Error for Error {}

/// Represent the name of a migration, which consists of a numeric version and a description.
///
/// The migration name is expected to contain the version and description separated by an
/// underscore, e.g., `version_description`. To improve the user experience, the version is intended
/// to be the current UNIX timestamp as this will allow us to create migrations on separate branches
/// without using a sequential numeric value.
///
/// There are still of course the possibility of conflicting migrations, but that will still be
/// possible regardless of the name structure.
///
/// The migration name will be ordered based on both the version and description, to ensure that the
/// order of the migrations are stable.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub(crate) struct MigrationName {
    version: i64,
    description: String,
}

impl MigrationName {
    pub(crate) fn version(&self) -> i64 {
        self.version
    }

    pub(crate) fn description(&self) -> &str {
        self.description.as_ref()
    }
}

impl MigrationName {
    /// Build a new migration name from its separate component, i.e., the version and description.
    pub(crate) fn new(version: i64, description: &str) -> Self {
        MigrationName {
            version,
            description: description.to_string(),
        }
    }

    /// Parse the migration name from a given value.
    pub(crate) fn parse(v: &str) -> Result<MigrationName, Box<dyn std::error::Error>> {
        v.split_once(MIGRATION_NAME_SEPARATOR)
            .ok_or(Error::UnableToParseWithInvalidFormat(v.to_string()))
            .and_then(|(v, description)| {
                v.parse::<i64>()
                    .map_err(|e| Error::UnableToParseVersion(v.to_string(), e))
                    .map(|version| MigrationName::new(version, description))
            })
            .map_err(|e| e.into())
    }
}

impl Display for MigrationName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}{}{}",
            self.version, MIGRATION_NAME_SEPARATOR, self.description
        )
    }
}

impl PartialEq<MigrationName> for &MigrationName {
    fn eq(&self, other: &MigrationName) -> bool {
        self.version == other.version && self.description == other.description
    }
}

#[cfg(test)]
#[macro_use]
mod tests {
    use crate::migration::name::MigrationName;
    use quickcheck::{quickcheck, Arbitrary, Gen};

    extern crate quickcheck;

    #[test]
    fn parse_with_empty_name() {
        let actual = MigrationName::parse("");

        assert!(actual.is_err());
    }

    #[test]
    fn parse_with_only_version() {
        let actual = MigrationName::parse("1");

        assert!(actual.is_err());
    }

    #[test]
    fn parse_with_only_description() {
        let actual = MigrationName::parse("_a");

        assert!(actual.is_err());
    }

    #[test]
    fn parse_with_valid_name() {
        let expected = MigrationName::new(1, "a");

        let actual = MigrationName::parse("1_a");

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn parse_with_long_description() {
        let expected = MigrationName::new(1, "a_b_c_d_e_f_g_h");

        let actual = MigrationName::parse("1_a_b_c_d_e_f_g_h");

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn sort_by_version() {
        let mut migrations = vec![MigrationName::new(2, "b"), MigrationName::new(1, "a")];
        let expected = vec![MigrationName::new(1, "a"), MigrationName::new(2, "b")];

        migrations.sort();

        assert_eq!(expected, migrations);
    }

    #[test]
    fn sort_by_description() {
        let mut migrations = vec![MigrationName::new(1, "b"), MigrationName::new(1, "a")];
        let expected = vec![MigrationName::new(1, "a"), MigrationName::new(1, "b")];

        migrations.sort();

        assert_eq!(expected, migrations);
    }

    #[test]
    fn sort_by_both_version_and_description() {
        let mut migrations = vec![
            MigrationName::new(2, "c"),
            MigrationName::new(1, "b"),
            MigrationName::new(4, "h"),
            MigrationName::new(4, "g"),
            MigrationName::new(3, "e"),
            MigrationName::new(3, "f"),
            MigrationName::new(2, "d"),
            MigrationName::new(1, "a"),
        ];
        let expected = vec![
            MigrationName::new(1, "a"),
            MigrationName::new(1, "b"),
            MigrationName::new(2, "c"),
            MigrationName::new(2, "d"),
            MigrationName::new(3, "e"),
            MigrationName::new(3, "f"),
            MigrationName::new(4, "g"),
            MigrationName::new(4, "h"),
        ];

        migrations.sort();

        assert_eq!(expected, migrations);
    }

    impl Arbitrary for MigrationName {
        fn arbitrary(g: &mut Gen) -> Self {
            MigrationName::new(
                i64::arbitrary(g),
                &format!("{}{}", String::arbitrary(g), char::arbitrary(g)),
            )
        }
    }

    quickcheck! {
        fn parse_name(lhs: MigrationName) -> bool {
            match MigrationName::parse(&format!("{}", lhs)) {
                Ok(rhs) => lhs == rhs,
                Err(_) => false
            }
        }

        fn sort(migrations: Vec<MigrationName>) -> bool {
            let mut lhs = migrations.clone();
            lhs.sort();
            lhs.reverse();

            let mut rhs = migrations.clone();
            rhs.sort();
            rhs.reverse();

            lhs == rhs
        }
    }
}
