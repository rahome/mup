/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt::{Display, Formatter};
use std::fs;
use std::fs::ReadDir;
use std::path::{Path, PathBuf};

use crate::migration::source::migration::AvailableMigration;
use crate::migration::source::migration_source::MigrationSource;
use crate::migration::source::script::Script;
use crate::migration::MigrationName;

#[derive(Debug, PartialEq)]
enum Error {
    UnableToReadMigrationDirectory(PathBuf, std::io::ErrorKind),
    UnableToReadMigrationDirectories(PathBuf, std::io::ErrorKind),
    FilenameIsNotAvailableInPath(PathBuf),
    UnableToConvertFilenameToString(std::ffi::OsString),
    UnableToReadScriptFile(PathBuf, std::io::ErrorKind),
    MigrationScriptIsEmpty(MigrationName),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnableToReadMigrationDirectory(path, e) => {
                write!(f, "Unable to read migration path {:?} ({})", path, e)
            }
            Error::UnableToReadMigrationDirectories(path, e) => {
                write!(
                    f,
                    "Unable to read directories from migration path {:?} ({})",
                    path, e
                )
            }
            Error::FilenameIsNotAvailableInPath(v) => {
                write!(f, "No filename is available in path {:?}", v)
            }
            Error::UnableToConvertFilenameToString(v) => {
                write!(f, "Unable to convert filename {:?} to string", v)
            }
            Error::UnableToReadScriptFile(v, e) => {
                write!(f, "Unable to read script file {:?} ({})", v, e)
            }
            Error::MigrationScriptIsEmpty(name) => {
                write!(f, "Script for migration \"{}\" is empty", name)
            }
        }
    }
}

impl std::error::Error for Error {}

pub(crate) struct FileMigrationSource {
    base_path: PathBuf,
}

impl FileMigrationSource {
    pub(crate) fn new(base_path: &Path) -> Self {
        FileMigrationSource {
            base_path: base_path.to_path_buf(),
        }
    }
}

impl MigrationSource for FileMigrationSource {
    fn migrations(&self) -> Result<Vec<AvailableMigration>, Box<dyn std::error::Error>> {
        let directory = fs::read_dir(&self.base_path)
            .map_err(|e| Error::UnableToReadMigrationDirectory(self.base_path.clone(), e.kind()))?;

        collect_migration_directories(&self.base_path, directory)
            .and_then(parse_migrations)
            .map(sort_migrations)
    }
}

fn collect_migration_directories(
    base_path: &Path,
    directory: ReadDir,
) -> Result<Vec<PathBuf>, Box<dyn std::error::Error>> {
    collect_directories(base_path, directory)
        .map(filter_valid_migrations)
        .map_err(|e| e.into())
}

fn collect_directories(base_path: &Path, directory: ReadDir) -> Result<Vec<PathBuf>, Error> {
    let contents = directory
        .into_iter()
        .map(|r| {
            r.map(|v| v.path()).map_err(|e| {
                Error::UnableToReadMigrationDirectories(base_path.to_path_buf(), e.kind())
            })
        })
        .collect::<Result<Vec<PathBuf>, Error>>()?;

    Ok(contents)
}

fn filter_valid_migrations(paths: Vec<PathBuf>) -> Vec<PathBuf> {
    paths
        .into_iter()
        .filter(|x| include_valid_migrations(x))
        .collect::<Vec<PathBuf>>()
}

fn include_valid_migrations(path: &Path) -> bool {
    path.is_dir() && path.join("up.sql").is_file()
}

fn parse_migrations(
    paths: Vec<PathBuf>,
) -> Result<Vec<AvailableMigration>, Box<dyn std::error::Error>> {
    paths.into_iter().map(|v| parse_migration(&v)).collect()
}

fn parse_migration(path: &Path) -> Result<AvailableMigration, Box<dyn std::error::Error>> {
    let filename = read_filename(path).map_err(Box::new)?;

    let migration_name = MigrationName::parse(&filename)?;
    let script = read_script(&migration_name, &path.join("up.sql"))?;
    Ok(AvailableMigration::new(migration_name, script))
}

fn read_filename(path: &Path) -> Result<String, Error> {
    path.file_name()
        .ok_or(Error::FilenameIsNotAvailableInPath(path.to_path_buf()))
        .and_then(|v| {
            v.to_str()
                .map(|x| x.to_string())
                .ok_or(Error::UnableToConvertFilenameToString(v.to_os_string()))
        })
}

fn read_script(migration_name: &MigrationName, path: &PathBuf) -> Result<Script, Error> {
    let content = fs::read_to_string(path)
        .map_err(|e| Error::UnableToReadScriptFile(path.to_path_buf(), e.kind()))?;

    if content.is_empty() {
        Err(Error::MigrationScriptIsEmpty(migration_name.to_owned()))
    } else {
        Ok(Script::new(content))
    }
}

fn sort_migrations(mut migrations: Vec<AvailableMigration>) -> Vec<AvailableMigration> {
    migrations.sort();
    migrations
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Write;
    use std::path::Path;

    use tempfile::TempDir;

    use crate::migration::source::script::Script;

    use super::*;

    fn create_tempdir() -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = TempDir::with_prefix("migrations")?;
        Ok(path.into_path())
    }

    fn create_dir(path: &Path) -> Result<PathBuf, Box<dyn std::error::Error>> {
        fs::create_dir(path)?;
        Ok(path.to_path_buf())
    }

    fn create_file(path: &Path, contents: String) -> Result<PathBuf, Box<dyn std::error::Error>> {
        let mut file = File::create(path)?;
        _ = file.write(contents.as_bytes())?;
        Ok(path.to_path_buf())
    }

    #[test]
    fn migrations_without_permission_to_migrations() {
        let base_path = PathBuf::from("/root");
        let source = FileMigrationSource::new(&base_path);

        let actual = source.migrations();

        assert!(actual.is_err());
    }

    #[test]
    fn migrations_without_migrations() -> Result<(), Box<dyn std::error::Error>> {
        let base_path = create_tempdir()?;
        let source = FileMigrationSource::new(&base_path);
        let expected: Vec<AvailableMigration> = vec![];

        let actual = source.migrations()?;

        assert_eq!(expected, actual);
        Ok(())
    }

    #[test]
    fn migrations_without_valid_migration() -> Result<(), Box<dyn std::error::Error>> {
        let base_path = create_tempdir()?;
        create_dir(&base_path.join("1_a"))?;
        let source = FileMigrationSource::new(&base_path);
        let expected: Vec<AvailableMigration> = vec![];

        let actual = source.migrations()?;

        assert_eq!(expected, actual);
        Ok(())
    }

    #[test]
    fn migrations_with_empty_migration() -> Result<(), Box<dyn std::error::Error>> {
        let base_path = create_tempdir()?;
        let a = create_dir(&base_path.join("1_a"))?;
        create_file(&a.join("up.sql"), "".to_string())?;
        let source = FileMigrationSource::new(&base_path);

        let actual = source.migrations();

        assert!(actual.is_err());
        Ok(())
    }

    #[test]
    fn migrations_with_migration() -> Result<(), Box<dyn std::error::Error>> {
        let base_path = create_tempdir()?;
        let a = create_dir(&base_path.join("1_a"))?;
        create_file(
            &a.join("up.sql"),
            "CREATE TABLE a (id BIGINT PRIMARY KEY);".to_string(),
        )?;
        let source = FileMigrationSource::new(&base_path);
        let expected = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script {
                content: "CREATE TABLE a (id BIGINT PRIMARY KEY);".to_string(),
                checksum: "7a5ff3bb3c91dcaa10c736b5c3d2e315a37f1a6d3e724a6ad47b49bf133545ce"
                    .to_string(),
            },
        )];

        let actual = source.migrations()?;

        assert_eq!(expected, actual);
        Ok(())
    }

    #[test]
    fn migrations_with_migrations() -> Result<(), Box<dyn std::error::Error>> {
        let base_path = create_tempdir()?;
        let a = create_dir(&base_path.join("1_a"))?;
        create_file(
            &a.join("up.sql"),
            "CREATE TABLE a (id BIGINT PRIMARY KEY);".to_string(),
        )?;
        let b = create_dir(&base_path.join("2_b"))?;
        create_file(
            &b.join("up.sql"),
            "ALTER TABLE a ADD COLUMN name TEXT NOT NULL;".to_string(),
        )?;
        let source = FileMigrationSource::new(&base_path);
        let expected = vec![
            AvailableMigration::new(
                MigrationName::new(1, "a"),
                Script {
                    content: "CREATE TABLE a (id BIGINT PRIMARY KEY);".to_string(),
                    checksum: "7a5ff3bb3c91dcaa10c736b5c3d2e315a37f1a6d3e724a6ad47b49bf133545ce"
                        .to_string(),
                },
            ),
            AvailableMigration::new(
                MigrationName::new(2, "b"),
                Script {
                    content: "ALTER TABLE a ADD COLUMN name TEXT NOT NULL;".to_string(),
                    checksum: "316c39691fac02a3ee68a1487bd19943f664babf714ac99b404bf37006272db5"
                        .to_string(),
                },
            ),
        ];

        let actual = source.migrations()?;

        assert_eq!(expected, actual);
        Ok(())
    }
}
