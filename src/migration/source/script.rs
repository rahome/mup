/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use sha256::digest;

#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) struct Script {
    pub(crate) content: String,
    pub(crate) checksum: String,
}

impl Script {
    pub(crate) fn new(content: String) -> Self {
        let checksum = digest(content.clone());
        Script {
            content: content.to_string(),
            checksum,
        }
    }

    pub(crate) fn from(content: &str) -> Self {
        Script::new(content.to_string())
    }
}
