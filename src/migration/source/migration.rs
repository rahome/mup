/*
 * mup
 * Copyright (C) 2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::migration::name::MigrationName;
use crate::migration::source::script::Script;
use crate::migration::Migration;
use std::cmp::Ordering;

/// Represents a migration available from a supported source.
///
/// The available migration consist of the migration name as well as the script for the migration,
/// which in turn also have a checksum associated with the script.
///
/// The available migration will be compared against the applied migration, where both the migration
/// name and the checksum is compared to ensure the migration integrity.
#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) struct AvailableMigration {
    name: MigrationName,
    script: Script,
}

impl AvailableMigration {
    pub(crate) fn content(&self) -> &str {
        self.script.content.as_ref()
    }
}

impl AvailableMigration {
    /// Build a new available migration from its separate component, i.e., the name and script.
    pub(crate) fn new(name: MigrationName, script: Script) -> Self {
        AvailableMigration { name, script }
    }
}

impl Migration for AvailableMigration {
    fn name(&self) -> &MigrationName {
        &self.name
    }

    fn checksum(&self) -> String {
        self.script.checksum.clone()
    }
}

impl PartialOrd for AvailableMigration {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for AvailableMigration {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

#[cfg(test)]
#[macro_use]
mod tests {
    use crate::migration::name::MigrationName;
    use crate::migration::source::migration::AvailableMigration;
    use crate::migration::source::script::Script;

    extern crate quickcheck;

    #[test]
    fn sort_by_version() {
        let mut actual = vec![
            AvailableMigration::new(MigrationName::new(8, "h"), Script::from("-- h")),
            AvailableMigration::new(MigrationName::new(4, "d"), Script::from("-- d")),
            AvailableMigration::new(MigrationName::new(10, "j"), Script::from("-- j")),
            AvailableMigration::new(MigrationName::new(5, "e"), Script::from("-- e")),
            AvailableMigration::new(MigrationName::new(6, "f"), Script::from("-- f")),
            AvailableMigration::new(MigrationName::new(7, "g"), Script::from("-- g")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
            AvailableMigration::new(MigrationName::new(9, "i"), Script::from("-- i")),
            AvailableMigration::new(MigrationName::new(3, "c"), Script::from("-- c")),
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
        ];
        let expected = vec![
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
            AvailableMigration::new(MigrationName::new(3, "c"), Script::from("-- c")),
            AvailableMigration::new(MigrationName::new(4, "d"), Script::from("-- d")),
            AvailableMigration::new(MigrationName::new(5, "e"), Script::from("-- e")),
            AvailableMigration::new(MigrationName::new(6, "f"), Script::from("-- f")),
            AvailableMigration::new(MigrationName::new(7, "g"), Script::from("-- g")),
            AvailableMigration::new(MigrationName::new(8, "h"), Script::from("-- h")),
            AvailableMigration::new(MigrationName::new(9, "i"), Script::from("-- i")),
            AvailableMigration::new(MigrationName::new(10, "j"), Script::from("-- j")),
        ];

        actual.sort();

        assert_eq!(expected, actual);
    }
}
