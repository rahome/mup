/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::migration::driver::migration::AppliedMigration;
use crate::migration::driver::postgres::PostgresDriver;
use crate::migration::name::MigrationName;
use crate::migration::source::migration::AvailableMigration;
use crate::migration::Migration;
use log::info;
use std::fmt::{Display, Formatter};

#[derive(Debug, PartialEq)]
enum Error {
    AppliedMigrationsAreMissingFromSource {
        number_of_applied_migrations: usize,
        number_of_available_migrations: usize,
    },
    IncorrectMigrationOrder(String, String),
    InvalidMigrationChecksum(MigrationName),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::AppliedMigrationsAreMissingFromSource {
                number_of_applied_migrations,
                number_of_available_migrations,
            } => {
                write!(
                    f,
                    "There are more migrations applied ({}) than are available ({}), \
                     have already applied migrations been removed?",
                    number_of_applied_migrations, number_of_available_migrations
                )
            }
            Error::IncorrectMigrationOrder(lhs, rhs) => {
                write!(
                    f,
                    "The migrations seems to be out of order when comparing the applied \
                     migrations and the migrations available on the filesystem\n\n\
                     Applied migration: {}\n\
                     Available migration: {}",
                    lhs, rhs
                )
            }
            Error::InvalidMigrationChecksum(name) => {
                write!(
                    f,
                    "Content for migration \"{}\" do not match applied migration",
                    name
                )
            }
        }
    }
}

impl std::error::Error for Error {}

pub(crate) struct Migrator {
    driver: PostgresDriver,
    pub(super) applied_migrations: Vec<AppliedMigration>,
}

impl Migrator {
    pub(super) fn new(driver: PostgresDriver, applied_migrations: Vec<AppliedMigration>) -> Self {
        Migrator {
            driver,
            applied_migrations,
        }
    }
}

impl Migrator {
    pub(crate) async fn apply(
        &mut self,
        available_migrations: Vec<AvailableMigration>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let pending_migrations =
            pending_migrations(&self.applied_migrations, available_migrations).map_err(Box::new)?;

        for pending_migration in pending_migrations {
            info!("Running migration \"{}\"", pending_migration.name());
            self.driver.run_migration(&pending_migration).await?
        }
        Ok(())
    }
}

fn pending_migrations(
    applied_migrations: &[AppliedMigration],
    available_migrations: Vec<AvailableMigration>,
) -> Result<Vec<AvailableMigration>, Error> {
    if applied_migrations.is_empty() {
        return Ok(available_migrations);
    }

    combine_migrations(available_migrations, applied_migrations)?
        .into_iter()
        .filter_map(|(lhs, rhs)| match rhs {
            Some(applied_migration) => {
                // If the migration have already been applied, we need to check that the order and
                // checksum is correct to ensure that we do not end up in an invalid state.
                is_migration_valid(lhs, applied_migration)
            }
            None => Some(Ok(lhs)),
        })
        .collect::<Result<Vec<AvailableMigration>, Error>>()
}

/// Describes the state for a given migration, the first part of the tuple is the available
/// migration and the second part is the applied migration, if the migration have been applied. If
/// the available migration have not yet been applied, the second argument will be [Option::None].
type MigrationState = (AvailableMigration, Option<AppliedMigration>);

/// Combine the available migrations with the already applied migrations based on their ordering.
///
/// The purpose of this function is to transform the migrations into a data structure that is easier
/// to check for potential issues. The resulting data structure is a [Vec] of tuples of two values,
/// where the first value is the available migration and the second value is the applied migration,
/// if one exists.
///
/// The resulting [Vec] is based on the order of the available migrations. The reason for this is
/// important as this will allow us to detect when a migration is missing based on its name, whether
/// the checksum between the available and applied migration differ, etc.
///
/// As we depend on the length of the available migrations, we also check whether the number of
/// available migrations are fewer than the number of applied migrations. This indicates that
/// applied migrations are no longer available.
fn combine_migrations(
    available_migrations: Vec<AvailableMigration>,
    applied_migrations: &[AppliedMigration],
) -> Result<Vec<MigrationState>, Error> {
    if applied_migrations.len() > available_migrations.len() {
        return Err(Error::AppliedMigrationsAreMissingFromSource {
            number_of_applied_migrations: applied_migrations.len(),
            number_of_available_migrations: available_migrations.len(),
        });
    }

    Ok(available_migrations
        .into_iter()
        .enumerate()
        .map(|(index, available_migration)| {
            (available_migration, applied_migrations.get(index).cloned())
        })
        .collect())
}

/// Determines whether the migration is valid.
///
/// The validation is done by checking the name of both the available and applied migration, which
/// indicate that the order of the migrations are correct, and the checksum which indicate whether
/// the content have changed.
fn is_migration_valid(
    available_migration: AvailableMigration,
    migration: AppliedMigration,
) -> Option<Result<AvailableMigration, Error>> {
    let migration_name = available_migration.name();
    if migration_name != migration.name() {
        Some(Err(Error::IncorrectMigrationOrder(
            migration.name().to_string(),
            migration_name.to_string(),
        )))
    } else if available_migration.checksum() != migration.checksum() {
        Some(Err(Error::InvalidMigrationChecksum(
            available_migration.name().clone(),
        )))
    } else {
        None
    }
}

//noinspection DuplicatedCode
#[cfg(test)]
mod tests {
    use crate::migration::driver::migration::AppliedMigration;
    use crate::migration::migrator::{combine_migrations, pending_migrations};
    use crate::migration::source::migration::AvailableMigration;
    use crate::migration::source::script::Script;
    use crate::migration::MigrationName;
    use std::time::Duration;

    #[test]
    fn pending_migrations_without_migrations() {
        let applied_migrations: Vec<AppliedMigration> = vec![];
        let available_migrations: Vec<AvailableMigration> = vec![];
        let expected: Vec<AvailableMigration> = vec![];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_ok());
        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn pending_migrations_with_pending_migration() {
        let applied_migrations: Vec<AppliedMigration> = vec![];
        let available_migrations: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];
        let expected: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_ok());
        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn pending_migrations_when_available_migrations_are_fewer_than_applied() {
        let applied_migrations: Vec<AppliedMigration> = vec![
            AppliedMigration::new(
                &MigrationName::new(1, "a"),
                "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
                Duration::from_nanos(2),
            ),
            AppliedMigration::new(
                &MigrationName::new(2, "b"),
                "530fa667f71656cb435a42c5aa1262596ad00d44fd57c77229bac6be00b1767c",
                Duration::from_nanos(2),
            ),
        ];
        let available_migrations: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_err());
    }

    #[test]
    fn pending_migrations_when_migration_is_missing() {
        let applied_migrations: Vec<AppliedMigration> = vec![AppliedMigration::new(
            &MigrationName::new(2, "a"),
            "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
            Duration::from_nanos(2),
        )];
        let available_migrations: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(1, "b"), Script::from("-- b")),
            AvailableMigration::new(MigrationName::new(2, "a"), Script::from("-- a")),
        ];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_err());
    }

    #[test]
    fn pending_migrations_with_incorrect_migration_order() {
        let applied_migrations: Vec<AppliedMigration> = vec![AppliedMigration::new(
            &MigrationName::new(2, "b"),
            "530fa667f71656cb435a42c5aa1262596ad00d44fd57c77229bac6be00b1767c",
            Duration::from_nanos(2),
        )];
        let available_migrations: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
        ];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_err());
    }

    #[test]
    fn pending_migrations_with_invalid_migration_checksum() {
        let applied_migrations: Vec<AppliedMigration> = vec![AppliedMigration::new(
            &MigrationName::new(1, "a"),
            "da7dc2da9bdb25cd2345182baebcc069243ddf2d90522b3c14cc497832de0b71",
            Duration::from_nanos(2),
        )];
        let available_migrations: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_err());
    }

    #[test]
    fn pending_migrations_with_pending_migrations() {
        let applied_migrations: Vec<AppliedMigration> = vec![AppliedMigration::new(
            &MigrationName::new(1, "a"),
            "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
            Duration::from_nanos(2),
        )];
        let available_migrations: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
            AvailableMigration::new(MigrationName::new(3, "c"), Script::from("-- c")),
        ];
        let expected: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
            AvailableMigration::new(MigrationName::new(3, "c"), Script::from("-- c")),
        ];

        let actual = pending_migrations(&applied_migrations, available_migrations);

        assert!(actual.is_ok());
        assert_eq!(expected, actual.unwrap());
    }

    // Combine migrations

    #[test]
    fn combine_migrations_without_available_migrations() {
        let available_migrations: Vec<AvailableMigration> = vec![];
        let applied_migrations: Vec<AppliedMigration> = vec![];
        let expected: Vec<(AvailableMigration, Option<AppliedMigration>)> = vec![];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }

    #[test]
    fn combine_migrations_when_available_migrations_are_missing() {
        let available_migrations: Vec<AvailableMigration> = vec![];
        let applied_migrations: Vec<AppliedMigration> = vec![AppliedMigration::new(
            &MigrationName::new(1, "a"),
            "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
            Duration::from_nanos(2),
        )];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_err());
    }

    #[test]
    fn combine_migrations_with_available_migration() {
        let available_migrations: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];
        let applied_migrations: Vec<AppliedMigration> = vec![];
        let expected: Vec<(AvailableMigration, Option<AppliedMigration>)> = vec![(
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            None,
        )];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }

    #[test]
    fn combine_migrations_with_available_migrations() {
        let available_migrations: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
        ];
        let applied_migrations: Vec<AppliedMigration> = vec![];
        let expected: Vec<(AvailableMigration, Option<AppliedMigration>)> = vec![
            (
                AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
                None,
            ),
            (
                AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
                None,
            ),
        ];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }

    #[test]
    fn combine_migrations_with_available_and_applied_migration() {
        let available_migrations: Vec<AvailableMigration> = vec![AvailableMigration::new(
            MigrationName::new(1, "a"),
            Script::from("-- a"),
        )];
        let applied_migrations = vec![AppliedMigration::new(
            &MigrationName::new(1, "a"),
            "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
            Duration::from_nanos(2),
        )];
        let expected: Vec<(AvailableMigration, Option<AppliedMigration>)> = vec![(
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            Some(AppliedMigration::new(
                &MigrationName::new(1, "a"),
                "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
                Duration::from_nanos(2),
            )),
        )];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }

    #[test]
    fn combine_migrations_with_available_and_applied_migrations() {
        let available_migrations: Vec<AvailableMigration> = vec![
            AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
            AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
        ];
        let applied_migrations = vec![
            AppliedMigration::new(
                &MigrationName::new(1, "a"),
                "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
                Duration::from_nanos(2),
            ),
            AppliedMigration::new(
                &MigrationName::new(2, "b"),
                "2487b93204cee32e1c2c8a68cd4740e7ef2cc1aa5090713c882d4eba1074c992",
                Duration::from_nanos(2),
            ),
        ];
        let expected: Vec<(AvailableMigration, Option<AppliedMigration>)> = vec![
            (
                AvailableMigration::new(MigrationName::new(1, "a"), Script::from("-- a")),
                Some(AppliedMigration::new(
                    &MigrationName::new(1, "a"),
                    "7e91c4a2d5ecc80b1be06084611a682814c40ab286d4de38158a76d0c08b603b",
                    Duration::from_nanos(2),
                )),
            ),
            (
                AvailableMigration::new(MigrationName::new(2, "b"), Script::from("-- b")),
                Some(AppliedMigration::new(
                    &MigrationName::new(2, "b"),
                    "2487b93204cee32e1c2c8a68cd4740e7ef2cc1aa5090713c882d4eba1074c992",
                    Duration::from_nanos(2),
                )),
            ),
        ];

        let result = combine_migrations(available_migrations, &applied_migrations);

        assert!(result.is_ok());
        assert_eq!(expected, result.unwrap());
    }
}
