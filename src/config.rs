/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::NAME;
use figment::providers::{Env, Format, Serialized, Toml};
use figment::value::{Dict, Map};
use figment::{Figment, Metadata, Profile, Provider};
use log::debug;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::path::{Path, PathBuf};

/// Define default path for the migrations.
const MIGRATIONS_PATH_DEFAULT: &str = "migrations";

/// Defines the prefix used for environment variables.
const ENVIRONMENT_VARIABLE_PREFIX: &str = "MUP_";

/// Errors related to how we work with configuration.
#[derive(Debug, PartialEq)]
enum Error {
    /// Indicate that we were unable to read the configuration from different source.
    UnableToReadConfiguration(figment::Error),
    /// Indicate that no database configuration is available.
    NoDatabaseConfigurationIsAvailable,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnableToReadConfiguration(e) => {
                write!(f, "{}", e)
            }
            Error::NoDatabaseConfigurationIsAvailable => {
                write!(
                    f,
                    "No database configuration is available, this can either be \
                     set using the \"database_url\" key in the configuration file \
                     or using the \"MUP_DATABASE_URL\" environment variable"
                )
            }
        }
    }
}

impl std::error::Error for Error {}

/// Define the properties of the external configuration.
#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub(crate) struct Config {
    /// Define path to the migrations directory, either relative from working directory or absolute.
    path: PathBuf,
    /// Define the configuration for Postgres including authentication and database name.
    database_url: Option<String>,
}

impl Config {
    pub(super) fn path(&self) -> &Path {
        self.path.as_ref()
    }
    pub(super) fn database_url(&self) -> Result<String, Box<dyn std::error::Error>> {
        match self.database_url.as_ref() {
            Some(database_url) => Ok(database_url.to_string()),
            None => Err(Box::new(Error::NoDatabaseConfigurationIsAvailable)),
        }
    }
}

impl Config {
    /// Parse configuration from supported sources, `toml` file or environment variables.
    pub(crate) fn parse(config_path: &str) -> Result<Self, Box<dyn std::error::Error>> {
        debug!("Parse configuration from {} and environment", config_path);
        let config = Figment::from(Config::default())
            .merge(Toml::file(config_path))
            .merge(Env::prefixed(ENVIRONMENT_VARIABLE_PREFIX))
            .extract()
            .map_err(|e| Box::new(Error::UnableToReadConfiguration(e)))?;

        Ok(config)
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            path: PathBuf::from(MIGRATIONS_PATH_DEFAULT),
            database_url: None,
        }
    }
}

impl Provider for Config {
    fn metadata(&self) -> Metadata {
        Metadata::named(format!("{} config", NAME))
    }

    fn data(&self) -> Result<Map<Profile, Dict>, figment::Error> {
        Serialized::defaults(self).data()
    }
}
