/*
 * mup
 * Copyright (C) 2023-2025 raatiniemi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::NAME;
use std::fmt::{Display, Formatter};
use std::path::PathBuf;

use crate::config::Config;
use clap::{Args, Parser};
use log::{debug, warn};

mod command;
mod create_command;
mod up_command;

/// Contains the short help text for the configuration path property.
const CONFIG_HELP: &str = "Path to the configuration file.";

/// Contains the long help text for the configuration path property.
const CONFIG_LONG_HELP: &str = r#"Path to the configuration file.

This argument specifies the location of the TOML configuration
file used to configure the application.

If not provided, the application will attempt to load a default
configuration from a predefined location."#;

/// Define the default path for the configuration file, will be used if one is not provided.
const CONFIG_DEFAULT: &str = ".mup.toml";

/// Contains the short about text for the create command.
const CREATE_ABOUT: &str = "Creates a new empty migration.";

/// Contains the long about text for the create command.
const CREATE_LONG_ABOUT: &str = r#"Creates a new empty migration.

This command will create a new empty migration, where the migration
directory name is based on the current UNIX timestamp in seconds,
followed by a user-provided description.

The directory will be created in the configured `path`, it will
contain a single empty file `up.sql`, allowing the user to manually
define the necessary SQL statements.

The structure for the migration will use the following format:

<path_to_migrations>/<timestamp>_<description>/up.sql

Spaces in the description will be replaced with underscores (`_`)
to simplify the navigation from the command line interface.

For example, if the current UNIX timestamp is `1705220178` and the
user runs the following command:

mup create --description "create users table"

The created directory and file structure will be:

<path_to_migrations>/1705220178_create_users_table/up.sql"#;

/// Contains the short help text for the description property associated with the create command.
const CREATE_DESCRIPTION_HELP: &str = "Description for the new migration.";

/// Contains the long help text for the description property associated with the create command.
const CREATE_DESCRIPTION_LONG_HELP: &str = r#"Description for the new migration.

The description, along with the current UNIX timestamp in seconds,
will be used for the directory containing the migration script."#;

/// Contains the short about text for the up command.
const UP_ABOUT: &str = "Runs all pending migrations.";

/// Contains the long about text for the up command.
const UP_LONG_ABOUT: &str = r#"Runs all pending migrations.

This command will collect the available and applied migrations,
compare the checksums and order to ensure the integrity of the
migrations, and then run all the pending migrations.

The content of each applied migration will be hashed using `sha256`
and persisted to allow for later comparison to ensure that an
already applied migration have not been changed.

In addition to the checksum verification, the migrations will be
ordered based on its' timestamp to ensure that the order of the
migrations remains the same."#;

/// Defines the possible errors that can occur when running the cli.
#[derive(Debug, PartialEq)]
enum Error {
    /// Indicate that the path passed as the configuration file is invalid, e.g., it might be a
    /// directory when a file is expected.
    InvalidConfigurationPath(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InvalidConfigurationPath(path) => {
                write!(
                    f,
                    "The configuration path \"{}\" is invalid, path is expected to be a file",
                    path
                )
            }
        }
    }
}

impl std::error::Error for Error {}

/// Command-line interface for the application.
///
/// This struct defines the available command-line arguments and tasks for creating new migrations
/// and running pending migrations against a database.
#[derive(Parser)]
#[command(name = NAME, long_about = "")]
struct CommandLine {
    /// Path to the configuration file.
    ///
    /// This argument specifies the location of the TOML configuration file used to configure the
    /// application. The file can contain properties defined by the [Config] struct.
    ///
    /// ```shell
    /// mup --config path/to/config.toml
    /// ```
    ///
    /// If not provided, the application will attempt to load a default configuration from a
    /// predefined location, i.e., [CONFIG_DEFAULT].
    /// ```
    #[arg(global = true, short, long, help = CONFIG_HELP, long_help = Some(CONFIG_LONG_HELP), default_value = CONFIG_DEFAULT)]
    config: String,

    #[command(subcommand)]
    command: Commands,
}

/// Define the available commands provided through the command-line application.
#[derive(Parser)]
#[command(name = NAME)]
#[command(bin_name = NAME)]
enum Commands {
    Create(CreateArgs),
    Up(UpArgs),
}

/// Creates a new empty migration.
///
/// This command will create a new empty migration, where the directory name is based on the
/// current UNIX timestamp in seconds, followed by a user-provided description.
#[derive(Args)]
#[command(author, version, about = CREATE_ABOUT, long_about = Some(CREATE_LONG_ABOUT))]
struct CreateArgs {
    /// Description that will be used for the new migration.
    #[arg(short, long, help = CREATE_DESCRIPTION_HELP, long_help = Some(CREATE_DESCRIPTION_LONG_HELP))]
    description: String,
}

/// Runs all pending migrations.
///
/// This command will collect the available and applied migrations, compare the checksums and order
/// to ensure the integrity of the migrations, and then run all the pending migrations.
#[derive(Args)]
#[command(author, version, about = UP_ABOUT, long_about = Some(UP_LONG_ABOUT))]
struct UpArgs {}

pub(super) async fn run() -> Result<(), Box<dyn std::error::Error>> {
    let cli = parse_cli()?;
    let config = Config::parse(&cli.config)?;
    command::create_command(cli, config)?.run().await
}

/// Parse the command line arguments.
fn parse_cli() -> Result<CommandLine, Box<dyn std::error::Error>> {
    debug!("Parse arguments from command line");
    ensure_cli_is_valid(CommandLine::parse()).map_err(|e| e.into())
}

/// Ensure that the command line arguments are valid.
fn ensure_cli_is_valid(cli: CommandLine) -> Result<CommandLine, Error> {
    debug!("Ensure that command line arguments are valid");
    if cli.config != CONFIG_DEFAULT {
        debug!("Using custom configuration path, validating...");
        let path = PathBuf::from(&cli.config);
        if !path.exists() {
            warn!(
                "Configuration path {} do not exists, using default...",
                cli.config
            );
            return Ok(cli);
        }
        if !path.is_file() {
            return Err(Error::InvalidConfigurationPath(cli.config));
        }
    }

    debug!("Command line arguments are valid");
    Ok(cli)
}

#[cfg(test)]
mod tests {
    use std::fs::File;

    use tempfile::TempDir;

    use crate::cli::{ensure_cli_is_valid, CommandLine, Commands, UpArgs, CONFIG_DEFAULT};

    #[test]
    fn ensure_cli_is_valid_with_defaults() {
        let cli = CommandLine {
            config: CONFIG_DEFAULT.to_string(),
            command: Commands::Up(UpArgs {}),
        };

        let actual = ensure_cli_is_valid(cli);

        assert!(actual.is_ok())
    }

    #[test]
    fn ensure_cli_is_valid_when_config_file_exists() -> Result<(), Box<dyn std::error::Error>> {
        let config_path = TempDir::with_prefix("config_")
            .map(|x| x.into_path())
            .map(|x| x.join(".mup.toml"))?;
        File::create(&config_path)?;
        let cli = CommandLine {
            config: config_path.into_os_string().into_string().unwrap(),
            command: Commands::Up(UpArgs {}),
        };

        let actual = ensure_cli_is_valid(cli);

        assert!(actual.is_ok());
        Ok(())
    }

    #[test]
    fn ensure_cli_is_valid_when_config_file_do_not_exists() {
        let cli = CommandLine {
            config: "/tmp/.mup.toml".to_string(),
            command: Commands::Up(UpArgs {}),
        };

        let actual = ensure_cli_is_valid(cli);

        assert!(actual.is_ok())
    }

    #[test]
    fn ensure_cli_is_valid_when_config_file_is_directory() {
        let cli = CommandLine {
            config: "/tmp".to_string(),
            command: Commands::Up(UpArgs {}),
        };

        let actual = ensure_cli_is_valid(cli);

        assert!(actual.is_err())
    }
}
