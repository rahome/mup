CURRENT_DIR = $(shell pwd)
USER_ID = $(shell id -u)

.PHONY: up

audit:
	cargo install cargo-audit 2> /dev/null
	cargo audit

fmt:
	rustup component add rustfmt 2> /dev/null
	cargo fmt --all

lint:
	rustup component add clippy 2> /dev/null
	cargo clippy --all-features --tests --examples -- -D clippy::all

detect-secrets-scan:
	docker run -w /app --user $(USER_ID) -it -v $(CURRENT_DIR):/app registry.gitlab.com/rahome/docker/detect-secrets:latest detect-secrets scan > .detect-secrets-baseline.json

detect-secrets-audit:
	docker run -w /app --user $(USER_ID) -it -v $(CURRENT_DIR):/app registry.gitlab.com/rahome/docker/detect-secrets:latest detect-secrets audit .detect-secrets-baseline.json

detect-secrets-check:
	docker run -w /app --user $(USER_ID) -it -v $(CURRENT_DIR):/app registry.gitlab.com/rahome/docker/detect-secrets:latest bash -c 'git ls-files -z | xargs -0 detect-secrets-hook --baseline .detect-secrets-baseline.json'

docker-compose-up:
	docker compose up -d

up: docker-compose-up

docker-compose-down:
	docker compose down --remove-orphans

down: docker-compose-down

cargo-test:
	cargo install cargo-llvm-cov 2> /dev/null
	cargo llvm-cov

test: cargo-test
